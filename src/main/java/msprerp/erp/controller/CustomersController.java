package msprerp.erp.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


@RestController
@RequestMapping("/customers")
public class CustomersController<url> {
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/")
    /*     http://localhost:8081/customers/ */
    public List<String> getCustomers() throws IOException {
        List<String> strings = new ArrayList<>();

        ResponseEntity<String> response = restTemplate
                .getForEntity("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers", String.class);

        if (response.getStatusCode().is2xxSuccessful()) {
            // parse the response and convert it to a list of strings
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode root = objectMapper.readTree(response.getBody());
            for (JsonNode customer : root) {
                strings.add(customer.get("username").asText());
            }
        } else {
            // handle the error
            int statusCode = response.getStatusCode().value();
            String statusText = response.getStatusCode().toString();
            throw new IOException(String.format("Failed to get customers from API: %d %s", statusCode, statusText));
        }

        return strings;

    }


    @GetMapping("/{id}/orders")
    /* http://localhost:8081/customers/2/orders */
    public ResponseEntity<List<Map<String, Object>>> getOrders(@PathVariable("id") Long customerId) {
        String url = "https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/" + customerId + "/orders";
        try {
            ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
            // Use ObjectMapper to parse the JSON response into a List of Maps
            ObjectMapper objectMapper = new ObjectMapper();
            List<Map<String, Object>> orders = objectMapper.readValue(response.getBody(), new TypeReference<List<Map<String, Object>>>(){});
            return new ResponseEntity<>(orders, HttpStatus.OK);
        } catch (IOException e) {
            // In case of an error, return an HTTP status of 500 (Internal Server Error)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{customerId}/orders/{orderId}/products")
   
   /*  http://localhost:8081/customers/2/orders/2/products */
    public ResponseEntity<List<Map<String, Object>>> getProducts(@PathVariable("customerId") Long customerId, @PathVariable("orderId") Long orderId) {
        String url = "https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/" + customerId + "/orders/" + orderId + "/products";
        try {
            ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
            // Use ObjectMapper to parse the JSON response into a List of Maps
            ObjectMapper objectMapper = new ObjectMapper();
            List<Map<String, Object>> products = objectMapper.readValue(response.getBody(), new TypeReference<List<Map<String, Object>>>(){});
            return new ResponseEntity<>(products, HttpStatus.OK);
        } catch (IOException e) {
            // In case of an error, return an HTTP status of 500 (Internal Server Error)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
