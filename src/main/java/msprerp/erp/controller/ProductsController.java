package msprerp.erp.controller;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/products")
public class ProductsController {
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/")
    /*     http://localhost:8081/products/ */
    public String getProducts() {
        String products = restTemplate.getForObject("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/products", String.class);
        return products;
    }

    @GetMapping("/{productId}")
    /*     http://localhost:8081/products/2 */
    public String getProduct(@PathVariable("productId") String productId) {
        String product = restTemplate.getForObject("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/products/{productId}", String.class, productId);
        return product;
    }

}
